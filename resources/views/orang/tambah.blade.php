@extends('template_admin.master')

@section('content')
<div class="card">
 
                  <div class="card-header">
                    <h4>Tabel Tambah Data Orang</h4>
                  </div>
                  <div class="card-body">
                  <form action="{{route('update_data')}}" method="post">
        @csrf
                    <!-- <div class="form-group row mb-3">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Id Orang</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" name="id" class="form-control">
                      </div>
                    </div> -->
                    <div class="card-body">
                    <div class="form-group row mb-3">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Orang</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" name="nama">
                      </div>
                    </div>
                    <div class="form-group row mb-3">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jenis Kelamin</label>
                      <div class="col-sm-12 col-md-7">
                        <select class="form-control selectric " name="jenis_kelamin">
                          <option>laki laki</option>
                          <option>perempuan</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row mb-3">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tgl Lahir</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="date" name="tgl_lahir" class="form-control">
                      </div>
                    </div>
                    <div class="form-group row mb-3">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Alamat</label>
                      <div class="col-sm-12 col-md-7">
                        <textarea name="alamat" class="summernote-simple"></textarea>
                      </div>
                    </div>
                    <div class="form-group row mb-3">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                      <div class="col-sm-12 col-md-7">
                        <button class="btn btn-primary">Tambah</button>
                      </div>
                    </div>
                    </form>
                  </div>
                 
                </div>
                @endsection