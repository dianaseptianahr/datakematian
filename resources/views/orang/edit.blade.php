@extends('template_admin.master')

@section('content')
<div class="card">
                  <div class="card-header">
                    <h4>Tabel Edit</h4>
                  </div>
                  <div class="card-body">
                    <form action="{{route('update_edit',$data->id)}}" method="post">
                      @csrf
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Id Orang</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" name="id" value="{{$data->id}}">
                      </div>
                    </div>
                    <div class="card-body">
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Orang</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" name="nama" value="{{$data->nama}}">
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" >Gender</label>
                      <div class="col-sm-12 col-md-7">
                        <select class="form-control selectric"name="jenis_kelamin" value="{{$data->jenis_kelamin}}">
                          <option>laki laki</option>
                          <option>perempuan</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tgl Lahir</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="date" class="form-control" name="tgl_lahir" value="{{$data->tgl_lahir}}">
                      </div>
                    </div>
                    <div class="card-body">
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Alamat</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" name="alamat" value="{{$data->alamat}}">
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                      <div class="col-sm-12 col-md-7">
                        <button href =""class="btn btn-primary">Tambah</button>
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
                @endsection