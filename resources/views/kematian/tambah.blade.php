@extends('template_admin.master')

@section('content')
<div class="card">
 
                  <div class="card-header">
                    <h4>Tabel Tambah Data Kematian</h4>
                  </div>
                  <div class="card-body">
                  <form action="{{route('updateDataKematian')}}" method="post">
        @csrf
                    <!-- <div class="form-group row mb-3">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Id Kematian</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" name="id_kematian" class="form-control">
                      </div>
                    </div> -->
                    <div class="card-body">
                    <div class="form-group row mb-3">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">id</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" name="id">
                      </div>
                    </div>
                    <div class="form-group row mb-3">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kasus</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" name="kasus">
                      </div>
                    </div>
                    <div class="form-group row mb-3">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tanggal Kematian</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="date" name="tgl_kematian" class="form-control">
                      </div>
                    </div>
                    <div class="form-group row mb-3">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">TKP</label>
                      <div class="col-sm-12 col-md-7">
                        <textarea name="tkp" class="summernote-simple"></textarea>
                      </div>
                    </div>
                    <div class="form-group row mb-3">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                      <div class="col-sm-12 col-md-7">
                        <button class="btn btn-primary">Tambah</button>
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
                @endsection