<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class orangModel extends Model
{
    protected $table = 'orang';
    protected $fillable = [
        'id','nama','jenis_kelamin','tgl_lahir','alamat','is_active',
    ];
}
