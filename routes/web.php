<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


// Route orang
Route::get('/Datakematian','orangController@dashboard');

Route::get('/orang','orangController@orang')->name('orang');

Route::get('/TambahData','orangController@TambahData')->name('TambahData');

Route::post('/update','orangController@postData')->name('update_data');

Route::get('edit/orang/{id}','orangController@editorang')->name('Edit_orang');

Route::post('update/{id}','orangController@update')->name('update_edit');

Route::get('softdelete/{id}','orangController@softDelete')->name('softdelete');


// route kematian 
Route::get('/kematian','kematianController@kematian')->name('kematian');

Route::get('/TambahDataKematian','kematianController@TambahData')->name('TambahDataKematian');

Route::post('/updateKematian','kematianController@postDataKematian')->name('updateDataKematian');

Route::get('/edit/kematian{id}','kematianController@editkematian')->name('editKematian');

Route::post('/updateKematian/{id}','kematianController@updateKematian')->name('update_editKematian');

Route::get('softdelete/kematian/{id}','kematianController@softDelete')->name('softdelete_kematian');

//Route index halaman utama

Route::get('/','orangController@index')->name('index');

//Route cariData

Route::get('/cari','orangController@CariData')->name('CariData');
